import axios from "axios";
import dotenv from "dotenv";

dotenv.config();
const apiIp =
  process.env.REACT_APP_NODE_ENV === "production"
    ? process.env.REACT_APP_API_IP_PRODUCTION2
    : process.env.REACT_APP_API_IP2;
const apiPort =
  process.env.REACT_APP_NODE_ENV === "production"
    ? process.env.REACT_APP_API_PORT_PRODUCTION2
    : process.env.REACT_APP_API_PORT2;
const Api = axios.create({
  baseURL: `http://${apiIp}:${apiPort}/`,
});

Api.defaults.withCredentials = true;

export default Api;
