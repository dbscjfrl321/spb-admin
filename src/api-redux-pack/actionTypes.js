export const FETCH_OVERVIEW = "api-redux-pack/FETCH_OVERVIEW";
export const FETCH_LIST = "api-redux-pack/FETCH_LIST";
export const FETCH_BANNER_LIST = "api-redux-pack/FETCH_BANNER_LIST";
export const FETCH_LOGIN_LOG_LIST = "api-redux-pack/FETCH_LOGIN_LOG_LIST";
export const RESET_LOGIN_LOG_LIST = "api-redux-pack/RESET_LOGIN_LOG_LIST";
export const FETCH_ERROR_LOG_LIST = "api-redux-pack/FETCH_ERROR_LOG_LIST";
export const RESET_ERROR_LOG_LIST = "api-redux-pack/RESET_ERROR_LOG_LIST";

export const FETCH = "api-redux-pack/FETCH";
export const UPDATE = "api-redux-pack/UPDATE";
export const CREATE = "api-redux-pack/CREATE";
export const RESET = "api-redux-pack/RESET";

export const LOGIN = "api-redux-pack/LOGIN";
export const LOGOUT = "api-redux-pack/LOGOUT";
export const LOGIN_SUCCESS = "api-redux-pack/LOGIN_SUCCESS";
export const LOGIN_FAILURE = "api-redux-pack/LOGIN_FAILURE";

export const RESET_BANNER = "api-redux-pack/RESET_BANNER";
export const UPLOAD_BANNER = "api-redux-pack/UPLOAD_BANNER";
export const UPDATE_BANNER = "api-redux-pack/UPDATE_BANNER";
export const DELETE_BANNER = "api-redux-pack/DELETE_BANNER";
