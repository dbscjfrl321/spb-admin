// AppNav
export const ADD_ADMIN_MODAL = "modal/ADD_ADMIN_MODAL";
export const ADD_BANNER = "modal/ADD_BANNER";

// BannerTable
export const UPDATE_BANNER = "modal/UPDATE_BANNER";
export const DELETE_BANNER = "modal/DELETE_BANNER";
