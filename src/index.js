import React from "react";
import ReactDOM from "react-dom";
import "./ui/app.css";
import SpbAdminApp from "./SpbAdminApp";

ReactDOM.render(<SpbAdminApp />, document.getElementById("root"));
